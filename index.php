<?php

require_once "lib/ThreeApi.php";
#require_once "vendor/phpquery/phpQuery/phpQuery.php";
require "vendor/autoload.php";

use lib\ThreeApi as ThreeApi;

$config = require("config.php");

$apiClient = new ThreeApi();
$apiClient->logUserIn($config['username'], $config['password']);
$balances = $apiClient->getBalance();

$jsonArray = array();

foreach ($balances as $balance) {
    $jsonArray[] = $balance->toDict();
}

header("Content-Type: application/json; charset=UTF-8");

echo json_encode(array(
    "balances" => $jsonArray
));

$apiClient->close();
