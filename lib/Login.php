<?php namespace lib;

require_once "LoginRequest.php";

use lib\LoginRequest;
use \phpQuery as phpQuery;

class Login {

    private $apiClient;
    private $mobileNumber;
    private $password;
    private $csrf;
    private $redirectUrl;
    private $loginUrl;

    public function __construct(ThreeApi $apiClient, $mobileNumber, $password) {
        $this->apiClient = $apiClient;
        $this->mobileNumber = $mobileNumber;
        $this->password = $password;
    }

    public function login() {
        $this->getLoginPage();

        $this->getLogin();
        $this->postLogin();

        #$html = $this->apiClient->get($this->redirectUrl);
        #var_dump($html);

        $html = $this->apiClient->get("https://my3account.three.ie/Home");
    }

    private function getLogin() {
        $html = $this->apiClient->get($this->loginUrl);
        $document = phpQuery::newDocument($html);
        $this->csrf = $document["form input[name=lt]"]->attr("value");
    }

    private function postLogin() {
        $request = new LoginRequest($this->mobileNumber, $this->password, $this->csrf);
        $html = $this->apiClient->post($this->loginUrl, $request->toArray());
        $document = phpQuery::newDocument($html);

        $this->redirectUrl = $document["a"]->attr("href");

        $html = $this->apiClient->get($this->redirectUrl);
    }

    private function getLoginPage() {
        $html = $this->apiClient->get("https://sso.three.ie/mylogin//login?auth-required=seamless&service=https%3A%2F%2Fmy3account.three.ie%2FLogin
        ");
        $doc = phpQuery::newDocument($html);
        $loginTicketUrl = $doc["a"]->attr("href");

        $html = $this->apiClient->get($loginTicketUrl);

        $doc = phpQuery::newDocument($html);
        $this->loginUrl = $doc["iframe"]->attr("src");
        
    }

}
