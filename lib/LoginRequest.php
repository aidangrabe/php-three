<?php namespace lib;

class LoginRequest {

    private $username;
    private $password;
    private $csrfToken;

    public function __construct($username, $password, $csrfToken) {
        $this->username = $username;
        $this->password = $password;
        $this->csrfToken = $csrfToken;
    }

    public function toArray() {
        return array(
            "username"  => $this->username,
            "password"  => $this->password,
            "lt"        => $this->csrfToken
        );
    }

}
