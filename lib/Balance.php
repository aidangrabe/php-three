<?php namespace lib;

class Balance {

    private $amount;
    private $expires;
    private $name;

    public function __construct() {
        
    }

    public function __get($var) {
        if ($this->$var != null) {
            return $this->$var;
        } else {
            return null;
        }
    }

    public function setAmount($amount) {
        $this->amount = trim($amount);
    }

    public function setExpiry($expiry) {
        // remove everthing but the date
        $this->expiry = preg_replace("/[^0-9\/]/", "", $expiry);
    }
    public function setName($name) {
        $this->name = trim($name);
    }

    public function toDict() {
        return array(
            "amount" => $this->amount,
            "expiry" => $this->expiry,
            "name" => $this->name
        );
    }

}
