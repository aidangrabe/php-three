<?php namespace lib;

require "Login.php";
require "Balance.php";

use \curl_close;
use \unlink;
use \curl_setopt;
use \phpQuery as phpQuery;

class ThreeApi {

    const API_ENDPOINT = "https://my3account.three.ie";
    const API_USER_AGENT = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.132 Safari/537.36";

    const GET_BALANCE = "/My_account_balance";

    // the curl handle
    private $ch;
    private $cookieJar;

    public function __construct() {
        $this->ch = curl_init(); 
        curl_setopt($this->ch, CURLOPT_COOKIEJAR, dirname(dirname(__FILE__)) . "/storage/cookiejar-" . time());
        $this->cookieJar = dirname(dirname(__FILE__)) . "/storage/cookiejar-" . time();
    }

    private function setDefaultHeaders() {
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.    8",
            "Connection: keep-alive",
            "Content-Type: application/x-www-form-urlencoded",
            "Accept-Language: en-GB,en-US;q=0.8,en;q=0.6",
            "DNT: 1"
        ));
        
        # curl_setopt($this->ch, CURLOPT_HEADER, true);
        # curl_setopt($this->ch, CURLINFO_HEADER_OUT, true);

        curl_setopt($this->ch, CURLOPT_ENCODING , "gzip,deflate,sdch");
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_USERAGENT, self::API_USER_AGENT);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($this->ch, CURLOPT_COOKIESESSION, true);
        curl_setopt($this->ch, CURLOPT_COOKIEJAR, $this->cookieJar);
        curl_setopt($this->ch, CURLOPT_REFERER, "https://sso.three.ie/mylogin/?service=https%3A%2F%2Fmy3account.three.ie%2FThreePortal%2Fappmanager%2FThree%2FMy3ROI%3F_nfpb%3Dtrue%26&dontTestForDongleUser=true");
    }

    public function get($url) {
        $this->setDefaultHeaders();
        curl_setopt($this->ch, CURLOPT_POST, false);
        curl_setopt($this->ch, CURLOPT_URL, $url);

        $response = curl_exec($this->ch);

        return $response;
    }

    public function post($url, $fields = array()) {
        $data = "";
        $amp = "";
        foreach ($fields as $key => $field) {
            $field = urlencode($field);
            $data .= "${amp}${key}={$field}";
            $amp = "&";
        }

        $this->setDefaultHeaders();
        curl_setopt($this->ch, CURLOPT_POST, count($fields));
        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $data);

        $response = curl_exec($this->ch);

        return $response;
    }

    public function close() {
        curl_close($this->ch);
        unlink($this->cookieJar);
    }

    public function logUserIn($mobileNumber, $password) {
        $login = new Login($this, $mobileNumber, $password);
        $login->login();
    }

    public function getBalance() {
        $values = array();
        $html = $this->get(self::API_ENDPOINT . self::GET_BALANCE);
        $doc = phpQuery::newDocument($html);
        $balances = $doc[".balance tr td"];
        foreach ($balances as $balance) {
            $balance = pq($balance);
            $values[] = $balance->text();
        }

        $balances = array();
        $balance = null;

        // -2 because we don't want the first 2 values in the $values array as they
        // are duplicated with more information later on
        $values = array_splice($values, 2, count($values));

        // create an array of Balance objects instead of a big list of values
        for ($i = 0; $i < count($values); $i++) {
            $type = $i % 3;
            switch ($type) {
                default:
                case 0:
                    if ($balance != null) {
                        $balances[] = $balance;
                    }
                    $balance = new Balance();
                    $balance->setName($values[$i]);
                    break;
                case 1:
                    $balance->setExpiry($values[$i]);
                    break;
                case 2:
                    $balance->setAmount($values[$i]);
                    break;
            }
        }
        return $balances;
    }

}
